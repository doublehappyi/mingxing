/**
 * Created by yishuangxi on 2015/12/23.
 */

(function ($) {
    $(function () {
        init();
        addEvents();
        console.log('a');
    });

    function init() {

    }

    function addEvents() {
        (function () {
            $("#m-content-box").scroll(function () {
                var $this = $(this),
                    viewH = $this.height(),//可见高度
                    contentH = $this.get(0).scrollHeight,//内容高度
                    scrollTop = $this.scrollTop();//滚动高度
                if (contentH - viewH - scrollTop <= 100) { //到达底部100px时,加载新内容
                    //if (scrollTop / (contentH - viewH) >= 0.95) { //到达底部100px时,加载新内容
                    scrollToBottom();
                }
            });
        })();
        //筛选
        $('#m-filter-box a').click(function () {
            var $this = $(this);
            if ($this.hasClass('btn-primary')) {
                $this.removeClass('btn-primary');
            } else {
                $this.addClass('btn-primary');
            }
        });

        $('#m-filter-confirm').click(function () {
            var url = getSchoolUrl();
            $('#m-school-list').empty();
            $('#m-school-loading').text('正在加载...').show();
            $('#m-filter-modal').removeClass('active');
            $.ajax(url, {
                success: function (data) {

                },
                error: function () {
                    //$('#m-school-loading-error').text('Sorry, 加载失败了，上拉重试！').show();
                },
                complete:function(){
                    $('#m-school-loading').hide();
                }
            });
        });

        $('#m-school-all').click(function () {

        });
    }

    function getSchoolUrl() {
        var $district = $('#m-district a.btn-primary'),
            $cate = $('#m-cate a.btn-primary');
        var district = [], cate = [];
        for (var i = 0; i < $district.length; i++) {
            district.push($district.eq(i).attr('data-dis'));
        }
        for (var j = 0; j < $cate.length; j++) {
            cate.push($cate.eq(j).attr('data-cate'));
        }

        return '/ajaxSchoolList?district=' + district.join(',') + '&cate=' + cate.join(',');
    }

    function scrollToBottom(page) {
        console.log('not at bottom');
    }

    function getSchoolList(url, callback) {
        $.ajax(url, {
            params: 1
        });
    }
})(jQuery);