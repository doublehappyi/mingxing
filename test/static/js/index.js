(function($){
    $(function(){
        $('#y-filter-btn').click(function(){
            $('#y-filter-box').toggle()
        });
        $('#y-filter-cancel').click(function(){
            $('#y-filter-box').hide()
        });
        $('#y-filter-confirm').click(function(){
            $('#y-filter-box').hide()
        });
    });
})(jQuery);