/**
 * Created by yishuangxi on 2015/12/29.
 */
var express = require('express');
var router = express.Router();
var tongkao = require('../models').tongkao;

router.get('/info', function (req, res, next) {
    tongkao.queryInfoList(function(err, rows, fields){
        var data = {};
        for(var i = 0; i < rows.length; i++){
            var row = rows[i];
            if (!data[row['district']]){
                data[row['district']] = [];
            }
            data[row['district']].push(row);
        }
        res.render('tongkao/info.html', {data:data});
    });
});

router.get('/info/:id', function (req, res, next) {
    var id = parseInt(req.params.id);
    tongkao.queryInfo(id, function(err, rows, fields){
        res.render('tongkao/content.html', {title:rows[0]['title'], content:rows[0]['content']});
    });
});

router.get('/admit', function (req, res, next) {
    tongkao.queryAdmitList(function(err, rows, fields){
        res.render('tongkao/admit.html', {rows:rows});
    });
});

router.get('/admit/:id', function (req, res, next) {
    var id = parseInt(req.params.id);
    tongkao.queryAdmit(id, function(err, rows, fields){
        res.render('tongkao/content.html', {title:rows[0]['title'], content:rows[0]['content']});
    });
});

router.get('/yktime', function (req, res, next) {
    res.render('tongkao/yktime.html');
});

router.get('/query', function (req, res, next) {
    tongkao.queryQueryList(function(err, rows, fields){
        if(err){res.json(err)}
        res.render('tongkao/query.html', {content:rows[0].content});
    });
});

module.exports = router;