var express = require('express');
var router = express.Router();
var models = require('../models');
router.get('/', function (req, res, next) {
    res.render('school/index.html');
});

router.get('/:id', function (req, res, next) {
    var school_id = req.params.id;
    console.log('school_id: ',school_id);
    res.render('school/school.html');
});

router.get('/intro/:id', function (req, res, next) {
    var id = req.params.id;
    var schoolModel = models.school;
    schoolModel.queryIntro(id, function(err, rows, fields){
        res.render('school/content.html', {title:"学校简介-" + rows[0]['name'], content:rows[0]['intro']});
    });
});

router.get('/enrollment/:id', function (req, res, next) {
    var id = req.params.id;
    var schoolModel = models.school;
    schoolModel.queryEnrollment(id, function(err, rows, fields){
        res.render('school/content.html', {title:rows[0]['title'], content:rows[0]['content']});
    });
});

router.get('/score/:id', function (req, res, next) {
    var id = req.params.id;
    var schoolModel = models.school;
    schoolModel.queryScore(id, function(err, rows, fields){
        res.render('school/content.html', {title:rows[0]['title'], content:rows[0]['content']});
    });
});

router.get('/major/:id', function (req, res, next) {
    var id = req.params.id;
    var schoolModel = models.school;
    schoolModel.queryMajor(id, function(err, rows, fields){
        res.render('school/content.html', {title:rows[0]['title'], content:rows[0]['content']});
    });
});

router.get('/ajax/schoolList', function (req, res, next) {
    var districtList = req.query.district && req.query.district.split(',') || [],
        cateList = req.query.cate && req.query.cate.split(',') || [],
        tab = parseInt(req.query.tab) || 0,
        page = req.query.page || 1;
    
    //console.log('districtList: ', req.query.district);
    //console.log('cateList: ', req.query.cate);
    //console.log('tab: ', req.query.tab);
    //console.log('page: ', req.query.page);

    //console.log(">>>>>>>>>>>>>districtList, cateList, tab, page:",districtList, cateList, tab, page);
    var schoolModel = models.school;
    schoolModel.querySchoolList(districtList, cateList, tab, page, function(err, rows, fields){
        res.json({
            retCode:0,
            data:{
                list:rows
            }
        });
    });

});

router.get('/ajax/schoolDetail', function (req, res, next) {
    var school_id = parseInt(req.query.school_id);
    console.log(">>>>>>>>>>>>>school_id:",school_id);
    var schoolModel = models.school;
    schoolModel.querySchoolDetail(school_id, function(err, rows, fields){
        res.json({
            retCode:0,
            data:{
                list:rows
            }
        });
    });

});

router.get('/ajax/majorList', function (req, res, next) {
    var school_id = parseInt(req.query.school_id);
    console.log(">>>>>>>>>>>>>school_id:",school_id);
    var schoolModel = models.school;
    schoolModel.queryMajorList(school_id, function(err, rows, fields){
        res.json({
            retCode:0,
            data:{
                list:rows
            }
        });
    });

});

router.get('/ajax/enrollmentList', function (req, res, next) {
    var school_id = parseInt(req.query.school_id);
    var schoolModel = models.school;
    schoolModel.queryEnrollmentList(school_id, function(err, rows, fields){
        res.json({
            retCode:0,
            data:{
                list:rows
            }
        });
    });
});

router.get('/ajax/scoreList', function (req, res, next) {
    var school_id = parseInt(req.query.school_id);
    var schoolModel = models.school;
    schoolModel.queryScoreList(school_id, function(err, rows, fields){
        res.json({
            retCode:0,
            data:{
                list:rows
            }
        });
    });
});

module.exports = router;
