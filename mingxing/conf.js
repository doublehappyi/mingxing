/**
 * Created by yishuangxi on 2015/12/23.
 */
var mysql = {
    host: 'localhost',
    user: 'root',
    password: '11111111',
    database: 'mx',
    charset: 'utf8',
    table: {
        district: 'district',
        school: 'school',
        school_cate: 'school_cate',
        school_and_cate: 'school_and_cate',
        school_major: 'school_major',
        school_major_cate: 'school_major_cate',
        school_enrollment: 'school_enrollment',
        school_score: 'school_score',
        tongkao_info:'tongkao_info',
        tongkao_admit:'tongkao_admit',
        tongkao_query:'tongkao_query'
    }
}

module.exports = {
    mysql: mysql
}

