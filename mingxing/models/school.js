/**
 * Created by yishuangxi on 2016/12/23.
 */

var dao = require('../dao');
var table = require('../conf').mysql.table;
var db = dao.db;
var swig = require('swig');
var mysql = require('mysql');

function querySchoolList(districtList, cateList, tab, page, callback) {
    var page = parseInt(page) || 1,
        callback=callback || function(){};
    var publish_year = 2016;
    var sql_str = 'select ' +
            'id, ' +
            'name, ' +
            'logo, ' +
            '(select count(id) from {{school_enrollment}} where school_id={{school}}.id and publish_year={{publish_year}}) as enrollment_flag ' +
            'from {{school}} where ',
        params = [],
        page_size = 40,
        start = (page - 1 ) * page_size;

    var districtListLen = districtList.length,
        cateListLen = cateList.length;

    console.log('districtListLen: ',districtListLen);
    if (districtListLen) {
        for(var i = 0; i < districtListLen;i++){
            if(i < districtListLen - 1){
                sql_str += 'district_id=? or '
            }else{
                sql_str += 'district_id=? and '
            }
        }
        console.log('districtListLen sql_str: ',sql_str);
        params = params.concat(districtList);
    }
    if (cateListLen) {
        var sql_cate_str = '';
        for(var i = 0; i < cateListLen; i++){
            if(i < (cateListLen - 1)){
                sql_cate_str +='school_cate_id=? or '
            }else{
                sql_cate_str +='school_cate_id=?'
            }
        }
        //从school_and_cate关系表中过滤出school_id出来，条件是类别
        sql_str += 'id in (select distinct(school_id) from {{school_and_cate}} where school_id={{school}}.id and '+sql_cate_str+') and ';
        params = params.concat(cateList);
    }
    //console.log('tab: ',tab);
    if (tab == 1) {
        //从2016年的简章表中过滤出
        sql_str += 'id in (select distinct(school_id) from {{school_enrollment}} where publish_year={{publish_year}}) and ';
    }

    sql_str += '1=1 order by id limit ?, ?';
    params = params.concat(start, page_size);

    sql_str = swig.compile(sql_str)({school: table.school, school_enrollment: table.school_enrollment, school_and_cate:table.school_and_cate, publish_year:publish_year});
    sql_str = mysql.format(sql_str, params);

    console.log('sql_str: ',sql_str);
    console.log('params: >>>>>>>>>>>>',params);

    db.query(sql_str, callback);
}

function querySchoolDetail(school_id, callback){
    var sql_str = 'select id, name, logo, banner, tag, schoolType, nature, ' +
        'recruitOrder, recruitTotalNum, address, telephone, city, ' +
        '(select name from {{district}} where id=school.district_id) as district ' +
        'from {{school}} where id=?'
    sql_str = swig.compile(sql_str)({school: table.school, district: table.district});
    sql_str = mysql.format(sql_str, [school_id]);
    db.query(sql_str, callback);
}

function queryMajorList(school_id, callback){
    var sql_str = 'select id, name, '+
        '(select name from {{school_major_cate}} where id={{school_major}}.school_major_cate_id) as school_major_cate ' +
        'from {{school_major}} where school_id=?';
    sql_str = swig.compile(sql_str)({school_major: table.school_major, school_major_cate: table.school_major_cate});
    sql_str = mysql.format(sql_str, [school_id]);
    db.query(sql_str, callback);
}

function queryEnrollmentList(school_id, callback){
    var sql_str = 'select id, title, publish_year ' +
        'from {{school_enrollment}} where school_id=?';
    sql_str = swig.compile(sql_str)({school_enrollment: table.school_enrollment});
    sql_str = mysql.format(sql_str, [school_id]);
    db.query(sql_str, callback);
}

function queryScoreList(school_id, callback){
    var sql_str = 'select id, title '+
        'from {{school_score}} where school_id=?';
    sql_str = swig.compile(sql_str)({school_score: table.school_score});
    sql_str = mysql.format(sql_str, [school_id]);
    db.query(sql_str, callback);
}

function queryIntro(id, callback){
    var sql_str = 'select intro from {{school}} where id=?';
    sql_str = swig.compile(sql_str)({school: table.school});
    sql_str = mysql.format(sql_str, [id]);
    db.query(sql_str, callback);
}

function queryEnrollment(id, callback){
    var sql_str = 'select title, content from {{school_enrollment}} where id=?';
    sql_str = swig.compile(sql_str)({school_enrollment: table.school_enrollment});
    sql_str = mysql.format(sql_str, [id]);
    db.query(sql_str, callback);
}

function queryScore(id, callback){
    var sql_str = 'select  title, content  from {{school_score}} where id=?';
    sql_str = swig.compile(sql_str)({school_score: table.school_score});
    sql_str = mysql.format(sql_str, [id]);
    db.query(sql_str, callback);
}

function queryMajor(id, callback){
    var sql_str = 'select name, content  from {{school_major}} where id=?';
    sql_str = swig.compile(sql_str)({school_score: table.school_score});
    sql_str = mysql.format(sql_str, [id]);
    db.query(sql_str, callback);
}

module.exports = {
    querySchoolList: querySchoolList,
    querySchoolDetail:querySchoolDetail,
    queryMajorList:queryMajorList,
    queryEnrollmentList:queryEnrollmentList,
    queryScoreList:queryScoreList,
    queryIntro:queryIntro,
    queryEnrollment:queryEnrollment,
    queryScore:queryScore,
    queryMajor:queryMajor
}