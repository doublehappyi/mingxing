/**
 * Created by yishuangxi on 2016/12/23.
 */

var dao = require('../dao');
var table = require('../conf').mysql.table;
var db = dao.db;
var swig = require('swig');
var mysql = require('mysql');

function queryInfoList(callback) {
    var sql_str = 'select id, title, content, type, district, datetime from {{tongkao_info}} order by district';
    sql_str = swig.compile(sql_str)({tongkao_info: table.tongkao_info});
    db.query(sql_str, callback);
}


function queryInfo(id, callback){
    var sql_str = 'select title, content from {{tongkao_info}} where id=?';
    sql_str = swig.compile(sql_str)({tongkao_info: table.tongkao_info});
    sql_str = mysql.format(sql_str, [id]);
    db.query(sql_str, callback);
}

function queryAdmitList(callback) {
    var sql_str = 'select id, title from {{tongkao_admit}} order by id';
    console.log('sql_str: ',sql_str);
    sql_str = swig.compile(sql_str)({tongkao_admit: table.tongkao_admit});
    db.query(sql_str, callback);
}

function queryAdmit(id, callback){
    var sql_str = 'select title, content from {{tongkao_admit}} where id=?';
    sql_str = swig.compile(sql_str)({tongkao_admit: table.tongkao_admit});
    sql_str = mysql.format(sql_str, [id]);
    db.query(sql_str, callback);
}

//function queryQueryList(){
//    var sql_str = 'select id, content from {{tongkao_query}}';
//    sql_str = swig.compile(sql_str)({tongkao_query: table.tongkao_query});
//    sql_str = mysql.format(sql_str);
//    console.log(sql_str);
//    db.query(sql_str, callback);
//}
function queryQueryList(callback) {
    var sql_str = 'select id, content from {{tongkao_query}}';
    console.log('sql_str: ',sql_str);
    sql_str = swig.compile(sql_str)({tongkao_query: table.tongkao_query});
    db.query(sql_str, callback);
}

module.exports = {
    queryInfoList: queryInfoList,
    queryInfo:queryInfo,
    queryAdmitList:queryAdmitList,
    queryAdmit:queryAdmit,
    queryQueryList:queryQueryList
}