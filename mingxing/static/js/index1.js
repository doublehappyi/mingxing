/**
 * Created by yishuangxi on 2015/12/23.
 */

(function ($) {
    $(function () {
        init();
        addEvents();
        console.log('a');
    });

    function init() {
        var url = getSchoolUrl(1);
        getSchoolList(url);
    }

    function addEvents() {
        var page = 2,
            page_size = 20;//翻页控制的全局变量
        (function () {
            var flag = false;
            $("#m-content-box").scroll(function () {
                //如果page被置为0了，则不再翻页了
                if (page == 0) return;
                var $this = $(this),
                    viewH = $this.height(),//可见高度
                    contentH = $this.get(0).scrollHeight,//内容高度
                    scrollTop = $this.scrollTop(),//滚动高度
                    url;
                if (contentH - viewH - scrollTop <= 100) { //到达底部100px时,加载新内容
                    //if (scrollTop / (contentH - viewH) >= 0.95) { //到达底部100px时,加载新内容
                    if (flag) return;
                    flag = true;
                    url = getSchoolUrl(page);
                    $('#m-school-loading').text('正在加载...').show();
                    getSchoolList(url, function (data) {
                        flag = false;
                        if (data.data.list.length < page_size) {
                            page = 0;
                            $('#m-school-loading').text('没有更多的数据啦...').show();
                        }else{
                            page++;
                            $('#m-school-loading').hide();
                        }
                    });
                }
            });
        })();

        //筛选
        $('#m-filter-box a').click(function () {
            var $this = $(this);
            if ($this.hasClass('btn-primary')) {
                $this.removeClass('btn-primary');
            } else {
                $this.addClass('btn-primary');
            }
        });

        $('#m-filter-confirm').click(function () {
            page=1;
            var url = getSchoolUrl(page);
            $('#m-school-list').empty();
            $('#m-school-loading').text('正在加载...').show();
            $('#m-filter-modal').removeClass('active');
            getSchoolList(url, function(){

            });
            //$.ajax(url, {
            //    success: function (data) {
            //
            //    },
            //    error: function () {
            //        //$('#m-school-loading-error').text('Sorry, 加载失败了，上拉重试！').show();
            //    },
            //    complete: function () {
            //        $('#m-school-loading').hide();
            //    }
            //});
        });

        $('#m-school-all').click(function () {

        });


        function getSchoolUrl(page) {
            var $district = $('#m-district a.btn-primary'),
                $cate = $('#m-cate a.btn-primary'),
                $enrollment = $('#m-school-2016');
            var district = [], cate = [], enrollment = '';
            for (var i = 0; i < $district.length; i++) {
                district.push($district.eq(i).attr('data-dis'));
            }
            for (var j = 0; j < $cate.length; j++) {
                cate.push($cate.eq(j).attr('data-cate'));
            }
            if ($enrollment.hasClass('active')) {
                enrollment = 1;
            }
            return '/school/ajax/schoolList?district=' + district.join(',') + '&cate=' + cate.join(',') + '&enrollment=' + enrollment + '&page=' + page;
        }

        function getSchoolList(url, callback) {
            var callback = callback || function () {};
            $('#m-school-loading').text('正在加载...').show();
            $.ajax(url, {
                success: function (data) {
                    if(data.retCode === 0){
                        var tpl = $('#ms-school-list').html();
                        var html = Mustache.render(tpl, data.data);
                        $('#m-school-list').append(html);
                        $('#m-school-loading').hide();
                    }else{
                        $('#m-school-loading').text('加载出错了，上拉重试').show();
                    }

                    callback(data);
                },
                error: function (err) {
                }
            });
        }
    }

})(jQuery);