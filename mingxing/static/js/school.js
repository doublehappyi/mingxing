/**
 * Created by yishuangxi on 2015/12/23.
 */

(function ($) {
    $(function () {
        var school_id = location.pathname.split('/').pop();

        renderSchoolDetail(school_id);
        renderMajorList(school_id);
        renderEnrollmentList(school_id);
        renderScoreList(school_id);
    });

    function renderSchoolDetail(school_id) {
        var url = '/school/ajax/schoolDetail?school_id=' + school_id;
        $.ajax(url).done(function (data) {
            if (data.retCode === 0) {
                if (data.data.list.length) {
                    var headerData = getSchoolHeaderData(data.data.list[0]);
                    $('#m-school-info').append(Mustache.render($('#ms-school-info').html(), data.data.list[0]));
                    $('#m-school-header').append(Mustache.render($('#ms-school-header').html(), headerData));
                }
            }
        }).fail(function (err) {
            $('#m-school-info').append('获取数据失败了')
        }).always();
    }

    function renderMajorList(school_id) {
        var url = '/school/ajax/majorList?school_id=' + school_id;
        $.ajax(url).done(function (data) {
            if (data.retCode === 0) {
                if (data.data.list.length) {
                    var newData = groupMajorData(data.data.list);
                    $('#m-school-major').append(Mustache.render($('#ms-school-major').html(), newData));
                }
            }
        }).fail(function (err) {

        }).always();
    }

    function renderEnrollmentList(school_id) {
        var url = '/school/ajax/enrollmentList?school_id=' + school_id;
        $.ajax(url).done(function (data) {
            if (data.retCode === 0) {
                var newData = getNewEnrollmentData(data.data.list);
                var oldData = getOldEnrollmentData(data.data.list);

                if (newData) {
                    $('#m-school-enrollment').append(Mustache.render($('#ms-school-enrollment').html(), newData));
                }
                if (oldData) {
                    $('#m-school-enrollment-old').append(Mustache.render($('#ms-school-enrollment-old').html(), oldData));
                }
            }
        }).fail(function (err) {

        }).always();
    }

    function renderScoreList(school_id) {
        var url = '/school/ajax/scoreList?school_id=' + school_id;
        $.ajax(url).done(function (data) {
            if (data.retCode === 0) {
                if (data.data.list && data.data.list.length > 0) {
                    $('#m-school-score').append(Mustache.render($('#ms-school-score').html(), data.data));
                }
            }
        }).fail(function (err) {

        }).always();
    }

    function groupMajorData(list) {
        var school_major_cate_list = [];

        for (var i = 0; i < list.length; i++) {
            var school_major_cate = list[i].school_major_cate;
            var is_cate_found = false;
            for (var k = 0; k < school_major_cate_list.length; k++) {
                if (school_major_cate_list[k] && (school_major_cate_list[k].cate == school_major_cate)) {
                    is_cate_found = true;
                    if (!school_major_cate_list[k].list) {
                        school_major_cate_list[k].list = [];
                    }
                    school_major_cate_list[k].list.push(list[i]);
                    break;
                }
            }
            if (!is_cate_found) {
                school_major_cate_list.push({
                    cate:school_major_cate,
                    list:[list[i]]
                });
            }
        }

        console.log('school_major_cate_list: ', school_major_cate_list);

        return {cate_list: school_major_cate_list};
    }

    function getSchoolHeaderData(data) {
        var new_data = {
            name: data.name,
            logo: data.logo,
            list: [data.district]
        };
        if (data.city) new_data.list.push(data.city);

        if (data.tag) {
            var tag_arr = data.tag.split(',')
            for (var i = 0; i < tag_arr.length; i++) {
                new_data.list.push(tag_arr[i]);
            }
        }

        return new_data;
    }

    function getNewEnrollmentData(list) {
        var new_list = [];
        for (var i = 0; i < list.length; i++) {
            if (list[i]['publish_year'] == 2016) {
                new_list.push(list[i]);
            }
        }
        if (new_list.length) {
            return {list: new_list};
        } else {
            return null;
        }
    }

    function getOldEnrollmentData(list) {
        var new_list = [];
        for (var i = 0; i < list.length; i++) {
            if (list[i]['publish_year'] != 2016) {
                new_list.push(list[i]);
            }
        }
        if (new_list.length) {
            return {list: new_list};
        } else {
            return null;
        }
    }

})(jQuery);