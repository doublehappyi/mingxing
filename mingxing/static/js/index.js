/**
 * Created by yishuangxi on 2015/12/23.
 */

(function ($) {
    $(function () {
        util.resetNav();
        util.ajax_school_list();
        addEvents();
    });

    function addEvents(){
        //筛选
        $('#m-filter-box a').click(function () {
            var $this = $(this);
            if ($this.hasClass('btn-primary'))
                $this.removeClass('btn-primary');
            else
                $this.addClass('btn-primary');
        });

        $('#m-filter-confirm').click(function () {
            page = 1;//重置页码
            $('#m-filter-modal').removeClass('active');//关闭筛选框
            //重新拉取数据
            util.ajax_school_list()
        });

        (function () {
            var flag = false;
            $("#m-content-box").scroll(function () {
                //如果page被置为0了，则不再翻页了
                if (page == 0) return;
                if (flag) return;
                var $this = $(this),
                    viewH = $this.height(),//可见高度
                    contentH = $this.get(0).scrollHeight,//内容高度
                    scrollTop = $this.scrollTop(),//滚动高度
                    url;
                if (contentH - viewH - scrollTop <= 100) { //到达底部100px时,加载新内容
                    //if (scrollTop / (contentH - viewH) >= 0.95) { //到达底部100px时,加载新内容
                    flag = true;
                    $('#m-school-loading').text('正在加载...').show();
                    util.ajax_school_list(function (data) {
                        flag = false;
                    });
                }
            });
        })();
    }

    var page = 1, page_size = 40;
    var tip = {
        show_loading: function (msg) {
            $('#m-school-tip').text(msg||'正在加载...').show();
        },
        show_error: function (msg) {
            $('#m-school-tip').text(msg||'加载出错啦！上拉重试').show();
        },
        show_finish: function (msg) {
            $('#m-school-tip').text(msg||'没有更多数据啦').show();
        },
        show_tip:function(msg){
            $('#m-school-tip').text(msg||'上拉加载更多').show();
        },
        hide: function () {
            $('#m-school-tip').hide();
        }
    };

    var util = {
        ajax_school_list: function (callback) {
            if(page === 0){
                tip.show_finish();return;
            }
            if(page === 1){
                $('#m-school-list').empty();
            }
            var self = this;
            var callback = callback || function () {
                };
            tip.show_loading();
            $.ajax(self.get_url())
                .done(function (data) {
                    if (data.retCode === 0) {
                        $('#m-school-list').append(Mustache.render($('#ms-school-list').html(), data.data));
                        if (data.data.list.length < page_size) {
                            tip.show_finish();
                            page = 0;
                        } else {
                            tip.show_tip();
                            page++;
                        }
                    } else {
                        tip.show_error();
                    }
                })
                .fail(function (err) {
                    callback(err);
                }).always(function(){
                    callback();
                });
        },
        get_url: function () {
            //取筛选值
            var dis = [], $dis = $('#m-district').find('.btn-primary');
            var cate = [], $cate = $('#m-cate').find('.btn-primary');
            for (var i = 0; i < $dis.length; i++) {
                dis.push($dis.eq(i).attr('data-value'));
            }
            for (var i = 0; i < $cate.length; i++) {
                cate.push($cate.eq(i).attr('data-value'));
            }
            dis = dis.join(',');
            cate = cate.join(',');

            var tab = util.get_query('tab')||0;
            return '/school/ajax/schoolList?' + 'tab=' + tab + '&district=' + dis + '&cate=' + cate+'&page='+page;
        },
        get_query: function (key) {
            var queryObj = [], tempArr,
                queryArr = location.search.replace('?', '').split('&');
            for (var i = 0; i < queryArr.length; i++) {
                tempArr = queryArr[i].split('=')
                queryObj[tempArr[0]] = tempArr[1]
            }
            return queryObj[key];
        },
        resetNav:function(){
            var self = this;
            var tab = parseInt(self.get_query('tab'))||0;
            $('.m-control-item').eq(tab).addClass('m-control-item-active');
        }
    }

})(jQuery);
