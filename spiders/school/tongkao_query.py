# coding:utf-8
import requests
from bs4 import BeautifulSoup
import sys

reload(sys)
sys.setdefaultencoding('utf8')
import db
import json
import re
import time

# ####################日志设置#####################
import os, utils
log_path = os.path.dirname(os.path.abspath(__file__)) + '/log'
logger = utils.get_logger(log_path)
# ####################日志设置#####################

def main():
    url = 'http://i.yikaoapp.com/d/282.html'
    html = utils.get_html(url)
    if not html:
        logger.warning('tong kao error')
        return
    tag = BeautifulSoup(html, 'html.parser').find('div','wrap')

    conn = db.get_connection()
    cursor = db.get_cursor(conn)

    try:
        cursor.execute('delete from tongkao_query')
        conn.commit()
        cursor.execute('''insert into tongkao_query values(null, %s)''', (tag, ))
        conn.commit()
    except Exception as e:
        logger.error('delete from tongkao_query error: EXCEPT {0}'.format(e))

    cursor.close()
    conn.close()
    # r_district = re.compile(r'[A-Z]-')
    # district = 'A-安徽省'
    # conn = db.get_connection()
    # cursor = db.get_cursor(conn)
    #
    # for c in tag.descendants:
    #     name = c.name
    #     text = str(c.string).strip().replace('&nbsp;', '')
    #     if name == 'a' and text!='':
    #         logger.debug("c: "+str(c))
    #         title, url = c.string, c['href']
    #         if not title or not url:
    #             logger.warning('title failed: {0},{1}'.format(title, url))
    #             continue
    #         html = utils.get_html(url).replace("'",'"')
    #         if not html:
    #             logger.warning('url request failed: {0}'.format(url))
    #             continue
    #         bs = BeautifulSoup(html, 'html.parser')
    #         if "更多" in title:
    #             items = bs.select('.wrap a')[1:]
    #             for item in items:
    #                 title, url = item.get_text(), item.attrs['href']
    #                 html = utils.get_html(url)
    #                 if not html:
    #                     logger.warning('url request failed: {0}'.format(url))
    #                     continue
    #                 content = str(BeautifulSoup(html, 'html.parser').select('.wrap')[0])
    #                 rows = select_tongkao_info(title, district, cursor)
    #                 if len(rows):
    #                     info_id = rows[0][0]
    #                     update_tongkao_info(content, info_id, cursor, conn)
    #                 else:
    #                     insert_tongkao_info(title, content, 1, district, cursor, conn)
    #         else:
    #             content = str(bs.select('.wrap')[0]).strip()
    #             rows = select_tongkao_info(title, district, cursor)
    #             if len(rows):
    #                 info_id = rows[0][0]
    #                 update_tongkao_info(content, info_id, cursor, conn)
    #             else:
    #                 insert_tongkao_info(title, content, 1, district, cursor, conn)
    #
    #     elif name is None and text != '':
    #         if '：' in text:
    #             title = text.split('：')[0].strip()
    #             content = text.split('：')[1].strip()
    #             rows = select_tongkao_info(title, district, cursor)
    #             if len(rows):
    #                 info_id = rows[0][0]
    #                 update_tongkao_info(content, info_id, cursor, conn)
    #             else:
    #                 insert_tongkao_info(title, content, 0, district, cursor, conn)
    #
    #         elif re.findall(r_district, text):
    #             district = text
    #
    # cursor.close()
    # conn.close()

# def select_tongkao_info(title, district, cursor):
#     try:
#         select_str = 'select id from {0} where title=%s and district=%s'.format(db.settings['table']['tongkao_info'])
#         cursor.execute(select_str, (title, district))
#         rows = cursor.fetchall()
#         logger.debug('rows: '+str(rows))
#         return rows
#     except Exception as e:
#         return []
#
# def update_tongkao_info(content, info_id, cursor, conn):
#     try:
#         update_str = 'update {0} as t set t.content=%s where id=%s'.format(db.settings['table']['tongkao_info'])
#         cursor.execute(update_str, (content, info_id))
#         conn.commit()
#     except Exception as e:
#         logger.error('update_tongkao_info: e: {0}'.format(e))
#
# def insert_tongkao_info(title, content, ttype, district, cursor, conn):
#     try:
#         insert_str = """insert into {0} VALUES (null, '{1}', '{2}',{3}, '{4}', {5})""".format(db.settings['table']['tongkao_info'], title, content, ttype, district, int(time.time()))
#         logger.debug("""insert into {0} VALUES (null, '{1}', '{2}',{3}, '{4}')""".format(db.settings['table']['tongkao_info'], title, ttype, district, int(time.time())))
#         cursor.execute(insert_str)
#         conn.commit()
#     except Exception as e:
#         logger.error('insert_tongkao_info: e: {0}'.format(e))

if __name__ == '__main__':
    main()
