###数据库
CREATE DATABASE IF NOT EXISTS mx DEFAULT CHARSET utf8 ;
use mx;


###地区表
CREATE TABLE IF NOT EXISTS district(
  id VARCHAR(10) NOT NULL PRIMARY KEY,
  name VARCHAR(100) UNIQUE DEFAULT NULL
);

###城市表
# CREATE TABLE IF NOT EXISTS city(
#   id VARCHAR(10) NOT NULL PRIMARY KEY,
#   name VARCHAR(100) DEFAULT NULL,
#   district_id VARCHAR(10) DEFAULT NULL
# );

###学校分类表
CREATE TABLE IF NOT EXISTS school_cate(
  id INT(10) unsigned NOT NULL auto_increment PRIMARY KEY,
  ref_id VARCHAR(10) UNIQUE NOT NULL,#网站上对应的id
  name VARCHAR(100) UNIQUE DEFAULT NULL
);

###学校表
CREATE TABLE IF NOT EXISTS school(
  id INT(10) unsigned NOT NULL auto_increment PRIMARY KEY ,
  ref_id INT(10) UNIQUE DEFAULT NULL , #网站上对应id
  name VARCHAR(100) UNIQUE DEFAULT NULL,
  logo VARCHAR(200) DEFAULT NULL,#学校校徽
  score VARCHAR(200) DEFAULT NULL,
  banner VARCHAR(200) DEFAULT NULL,#学校详情页top展示图
  tag VARCHAR(100) DEFAULT NULL,###学校标签
  schoolType VARCHAR(100) DEFAULT NULL,###学校类型
  nature VARCHAR(100) DEFAULT NULL,###学校性质
  recruitOrder VARCHAR(100) DEFAULT NULL,###录取批次
  recruitTotalNum VARCHAR(100) DEFAULT NULL, #招生总人数
  intro MEDIUMTEXT DEFAULT NULL,###学校简介
  address VARCHAR(100) DEFAULT NULL,###地址
  telephone VARCHAR(100) DEFAULT NULL,###电话
  city VARCHAR(100) DEFAULT NULL, #城市名字

  district_id VARCHAR(10) DEFAULT NULL #地区id, 外键
);

###历届简章表
CREATE TABLE IF NOT EXISTS school_enrollment(
  id INT(10) unsigned NOT NULL auto_increment PRIMARY KEY ,
  ref_id INT(10) UNIQUE NOT NULL,
  title VARCHAR(200) DEFAULT NULL ,
  content MEDIUMTEXT DEFAULT NULL,
  school_id INT(10) DEFAULT NULL,
  publish_year INT(4) DEFAULT 0
);

###专业类目表：专业类目不从属于任何学校或者地区
CREATE TABLE IF NOT EXISTS school_major_cate(
  id INT(10) unsigned NOT NULL auto_increment PRIMARY KEY ,
  name VARCHAR(100) UNIQUE DEFAULT NULL
);
#插入金泰数据
INSERT INTO school_major_cate VALUES (1, '播音');
INSERT INTO school_major_cate VALUES (2, '编导');
INSERT INTO school_major_cate VALUES (3, '表演');
INSERT INTO school_major_cate VALUES (4, '摄影');
INSERT INTO school_major_cate VALUES (5, '美术');
INSERT INTO school_major_cate VALUES (6, '舞蹈');
INSERT INTO school_major_cate VALUES (7, '音乐');
INSERT INTO school_major_cate VALUES (8, '服表');


###专业表
CREATE TABLE IF NOT EXISTS school_major(
  id INT(10) unsigned NOT NULL auto_increment PRIMARY KEY ,
  name VARCHAR(100) DEFAULT NULL,
  school_id INT(10) DEFAULT NULL,
  school_major_cate_id INT(10) DEFAULT NULL
);

###历届参考分数表
CREATE TABLE IF NOT EXISTS school_score(
  id INT(10) unsigned NOT NULL auto_increment PRIMARY KEY ,
  ref_id INT(10) UNIQUE NOT NULL,
  title VARCHAR(200) DEFAULT NULL,
  content MEDIUMTEXT DEFAULT NULL,
  school_id INT(10) DEFAULT NULL
);

###最新消息
# CREATE TABLE IF NOT EXISTS school_news(
#   id INT(10) unsigned NOT NULL auto_increment PRIMARY KEY ,
#   title VARCHAR(200) DEFAULT NULL,
#   content MEDIUMTEXT DEFAULT NULL,
#   school_id INT(10) DEFAULT NULL
# );

#学校和学校分类关系表：一个学校属于多个分类
CREATE TABLE IF NOT EXISTS school_and_cate(
  id INT(10) unsigned NOT NULL auto_increment PRIMARY KEY ,
  school_id INT(10) DEFAULT NULL,
  school_cate_id INT(10) NOT NULL,
  CONSTRAINT UNIQUE (school_cate_id, school_id)
);



#统考信息汇总
# CREATE TABLE IF NOT EXISTS tongkao_district(
#   id INT(10) unsigned NOT NULL auto_increment PRIMARY KEY ,
#   name VARCHAR (100) UNIQUE NOT NULL
# );

CREATE TABLE IF NOT EXISTS tongkao_info(
  id INT(10) unsigned NOT NULL auto_increment PRIMARY KEY ,
  title VARCHAR(300) DEFAULT NULL,
  content MEDIUMTEXT NOT NULL,
  type TINYINT(1) DEFAULT 0, #0是简单类型，1是复杂类型
  district VARCHAR(100) NOT NULL,
  datetime INT(10) NOT NULL
);


CREATE TABLE IF NOT EXISTS tongkao_admit(
  id INT(10) unsigned NOT NULL auto_increment PRIMARY KEY ,
  title VARCHAR(200) UNIQUE NOT NULL,
  content MEDIUMTEXT DEFAULT NULL
);

#统考信息查询
CREATE TABLE IF NOT EXISTS tongkao_query(
  id INT(10) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY ,
  content MEDIUMTEXT DEFAULT NULL
);


