# coding:utf-8
import requests
from bs4 import BeautifulSoup
import sys

reload(sys)
sys.setdefaultencoding('utf8')
import db
import json
from datetime import date

# ####################日志设置#####################
import os, utils

log_path = os.path.dirname(os.path.abspath(__file__)) + '/log'
logger = utils.get_logger(log_path)


# ####################日志设置#####################



def main():
    # 从数据库中获取所有学校id, ref_id数据
    conn = db.get_connection()
    cursor = db.get_cursor(conn)
    cursor.execute('select id, content from {0} ORDER BY id'.format(db.settings['table']['school_enrollment']))
    print '正在更新数据库...'
    for row in cursor:
        school_id = row[0]
        content = row[1].replace('src="/', 'src="http://i.yikaosheng.com/')
        update_str = 'update school_enrollment set content=%s where id=%s'
        cursor.execute(update_str, (content, school_id))
        conn.commit()

    # for school in cursor:
    #     school_id, school_ref_id = school
    #
    #     logger.debug('school_id: ' + str(school_id) + '\t' + 'school_ref_id: ' + str(school_ref_id))
    #     school_url = 'http://i.yikaosheng.com/school/school_' + str(school_ref_id) + '.html'
    #
    #     html = utils.get_html(school_url)
    #     if not html:
    #         continue
    #     bs = BeautifulSoup(html, 'html.parser')
    #
    #     # 处理简章数据
    #     new_enrollment_list = get_new_enrollment_list(bs, school_url)
    #     for new_enrollment_item in new_enrollment_list:
    #         ref_id, title, content = new_enrollment_item
    #         select_str = 'select id from {0} where ref_id=%s'.format(db.settings['table']['school_enrollment'])
    #         if cursor.execute(select_str, (ref_id,)):
    #             logger.warning('enrollment_ref_id {0} is already exists !'.format(ref_id))
    #             continue
    #
    #         logger.debug('insert: ref_id: {0}, title: {1}, school_id: {2}'.format(ref_id, title, school_id))
    #         insert_str = 'insert into {0} VALUES (NULL, %s, %s, %s, %s, %s)'.format(
    #                 db.settings['table']['school_enrollment'])
    #         cursor.execute(insert_str, (ref_id, title, content, school_id, 2016))
    #         conn.commit()

            # 处理分数线数据

    conn.close()
    cursor.close()


if __name__ == '__main__':
    main()
