#coding:utf-8
import district
import school_cate
import school_basic
import school_update_district
import school_update_cate
import school_update_basic
import school_major
import school_enrollment_old_and_score
import school_enrollment_new

if __name__ == '__main__':
    district.main() #获取地区表数据 ，  一般只需要执行一次
    school_cate.main() #获取学校分类表数据，  一般只需要执行一次
    school_basic.main() #获取学校基本信息表数据，  一般只需要执行一次
    school_update_district.main() #更新学校表地区id，  一般只需要执行一次
    school_update_cate.main() #获取学校-学校类别关系表数据（一对多），  一般只需要执行一次
    school_update_basic.main() #获取学校基本信息表更多数据，  一般只需要执行一次
    school_major.main() #获取专业信息
    school_enrollment_old_and_score.main() #获取历年招生简章以及历年分数线表
    school_enrollment_new.main() #更新最新的招生简章信息

