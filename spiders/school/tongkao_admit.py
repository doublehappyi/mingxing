# coding:utf-8
from bs4 import BeautifulSoup
import sys

reload(sys)
sys.setdefaultencoding('utf8')
import db
# ####################日志设置#####################
import os, utils
log_path = os.path.dirname(os.path.abspath(__file__)) + '/log'
logger = utils.get_logger(log_path)
# ####################日志设置#####################

def main():
    url = 'http://i.yikaoapp.com/d-51.html'
    html = utils.get_html(url)

    if not html:
        logger.warning('tongkao_admit error')
        return

    bs = BeautifulSoup(html, 'html.parser')

    items = bs.select('.wrap a')
    conn = db.get_connection()
    cursor = db.get_cursor(conn)
    for item in items:
        url = item.attrs['href']
        html2 = utils.get_html(url)
        if not html2:
            logger.warning('tongkao_admit error')
            continue
        bs2 = BeautifulSoup(html2, 'html.parser')
        title = bs2.select('title')[0].get_text()
        content = str(bs2.select('.wrap')[0])
        logger.debug('content: '+ content.decode('utf-8'))
        select_str = 'select id from {0} where title=%s'.format(db.settings['table']['tongkao_admit'])
        if cursor.execute(select_str, (title,)):
            logger.warning('title: {0} is already exists'.format(title))
            continue
        else:
            logger.debug("insert into {0} values(null, {1})".format(db.settings['table']['tongkao_admit'], title))
            insert_str = "insert into {0} values(null, %s, %s)".format(db.settings['table']['tongkao_admit'])
            cursor.execute(insert_str, (title, content))
            conn.commit()

if __name__ == '__main__':
    main()