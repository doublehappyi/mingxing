# coding:utf-8
import requests
from bs4 import BeautifulSoup
import sys

reload(sys)
sys.setdefaultencoding('utf8')
import db

# ####################日志设置#####################
import os, utils

log_path = os.path.dirname(os.path.abspath(__file__)) + '/log'
logger = utils.get_logger(log_path)


# ####################日志设置#####################

def main():
    url = 'http://www.yikaosheng.com/school/'

    html = utils.get_html(url)
    if not html:
        return
    bs = BeautifulSoup(html, 'html.parser')
    items = bs.select('#spc_txt_sel a')[1:]

    conn = db.get_connection()
    cursor = db.get_cursor(conn)
    for item in items:
        school_cate_ref_id = item.attrs['__spc']
        school_cate_name = item.get_text()
        logger.debug(school_cate_ref_id + '\t' + school_cate_name)
        select_str = "select ref_id from {0}".format(db.settings['table']['school_cate']) + " where ref_id=%s"
        if cursor.execute(select_str, (school_cate_ref_id,)):
            logger.warning('ref_id: {0} exists'.format(school_cate_ref_id))
            continue
        insert_str = "insert into {0}".format(db.settings['table']['school_cate']) + " values (null, %s, %s)"
        cursor.execute(insert_str, (school_cate_ref_id, school_cate_name))
        conn.commit()

    conn.close()
    cursor.close()


if __name__ == '__main__':
    main()
