#coding:utf-8
import sys
reload(sys)
sys.setdefaultencoding('utf8')
__author__ = 'yishuangxi'
import MySQLdb

settings = {
    'host':'localhost',
    'user':'root',
    'passwd':'11111111',
    'db':'mx',
    'charset':'utf8',
    'table':{
        'district':'district',
        'school':'school',
        'school_cate':'school_cate',
        'school_and_cate':'school_and_cate',
        'school_major':'school_major',
        'school_major_cate':'school_major_cate',
        'school_enrollment':'school_enrollment',
        'school_score':'school_score',
        'tongkao_info':'tongkao_info',
        'tongkao_admit':'tongkao_admit'
    }
}

def get_connection():
    return MySQLdb.connect(host=settings['host'], user=settings['user'], passwd=settings['passwd'], db=settings['db'],charset=settings['charset'])

def get_cursor(conn):
    return conn.cursor()