# coding:utf-8
import requests
from bs4 import BeautifulSoup
import sys

reload(sys)
sys.setdefaultencoding('utf8')
import db
import json

# ####################日志设置#####################
import os, utils

log_path = os.path.dirname(os.path.abspath(__file__)) + '/log'
logger = utils.get_logger(log_path)
# ####################日志设置#####################

import time


def main():
    basic_url = 'http://www.yikaosheng.com/school/ajax.php?score=&spc=&my_city=&my_kaodian=&my_chengji=&my_fav=&key=&dis='

    conn = db.get_connection()
    cursor = db.get_cursor(conn)

    cursor.execute('select id from {0}'.format(db.settings['table']['district']))
    districtList = cursor.fetchall()

    for district in districtList:
        district_id = district[0]

        # 遍历该地区下的所有学校
        page = 1
        while 1:
            url = basic_url + str(district_id)
            url = url + '&page=' + str(page)
            logger.debug('url: {0}'.format(url))
            try:
                data = utils.get_html(url)
                if not data:
                    continue
                data = json.loads(data)[0:-1]
                for item in data:
                    ref_id = item['id']
                    # 判断school表里面是否有该学校
                    select_str = "select id from {0}".format(db.settings['table']['school']) + " where ref_id=%s"
                    if not cursor.execute(select_str, (ref_id,)):
                        logger.warning('ref_id: {0} is not exists'.format(ref_id))
                        continue
                    # 如果有，更新其district_id
                    logger.debug('update: ref_id: {0}, district_id: {1}'.format(ref_id, district_id))
                    update_str = 'update {0} set district_id=%s where ref_id=%s'.format(db.settings['table']['school'])
                    cursor.execute(update_str, (district_id, ref_id))
                    conn.commit()
                page += 1
                if len(data) > 1:
                    continue
                else:
                    break
            except Exception as e:
                logger.error('Error url :{0}, e:{1}'.format(url, e))

    conn.close()
    cursor.close()


if __name__ == '__main__':
    main()
