# coding:utf-8
import requests
from bs4 import BeautifulSoup
import sys

reload(sys)
sys.setdefaultencoding('utf8')
import db

# ####################日志设置#####################
import os, utils

log_path = os.path.dirname(os.path.abspath(__file__)) + '/log'
logger = utils.get_logger(log_path)


# ####################日志设置#####################

def main():
    url = 'http://www.yikaosheng.com/school/'
    html = utils.get_html(url)
    if not html:
        return
    bs = BeautifulSoup(html, 'html.parser')
    items = bs.select('#scl_nav a')[3:]

    conn = db.get_connection()
    cursor = db.get_cursor(conn)
    for item in items:
        dis_id = item.attrs['__dis']
        dis_name = item.get_text()
        logger.debug(dis_id + '\t' + dis_name)
        select_str = "select id from {0}".format(db.settings['table']['district']) + " where id=%s"
        if cursor.execute(select_str, (dis_id,)):
            logger.warning('id: {0} exists'.format(dis_id))
            continue
        insert_str = "insert into {0}".format(db.settings['table']['district']) + " values (%s, %s)"
        cursor.execute(insert_str, (dis_id, dis_name))
        conn.commit()

    conn.close()
    cursor.close()


if __name__ == '__main__':
    main()
