# coding:utf-8
import requests
from bs4 import BeautifulSoup
import sys

reload(sys)
sys.setdefaultencoding('utf8')
import db
import json

# ####################日志设置#####################
import os, utils

log_path = os.path.dirname(os.path.abspath(__file__)) + '/log'
logger = utils.get_logger(log_path)


# ####################日志设置#####################

def get_majorBoxItem(bs):
    item = None
    s_tips = bs.select('.s_tips')
    for tip in s_tips:
        if tip.get_text() == '招生专业':
            item = bs.select('.s_top2')[0]
            break
    return item


def main():
    # 从数据库中获取所有学校id, ref_id数据
    conn = db.get_connection()
    cursor = db.get_cursor(conn)
    cursor.execute('select id, ref_id from {0} ORDER BY id'.format(db.settings['table']['school']))
    schoolList = cursor.fetchall()
    for school in schoolList:
        school_id, school_ref_id = school

        logger.debug('school_id: ' + str(school_id) + '\t' + 'school_ref_id: ' + str(school_ref_id))
        school_url = 'http://i.yikaosheng.com/school/school_' + str(school_ref_id) + '.html'

        html = utils.get_html(school_url)
        if not html:
            continue
        bs = BeautifulSoup(html, 'html.parser')

        # 处理专业数据
        s_item_wraps = bs.select('.s_item_wrap > .s_item')
        for s_item_wrap in s_item_wraps:
            school_major_cate_id = int(s_item_wrap.attrs['__d'])
            name = s_item_wrap.select('a')[0].get_text().strip()

            select_str = 'select id from {0} where name=%s and school_id=%s and school_major_cate_id=%s limit 1'.format(
                    db.settings['table']['school_major'])
            if cursor.execute(select_str, (name, school_id, school_major_cate_id)):
                logger.warning(
                        'major: name: {0}, school_id: {1}, school_major_cate_id: {2} exists !'.format(name, school_id,
                                                                                                      school_major_cate_id))
                continue
            logger.debug('insert:  name: {0}, school_id: {1}, school_major_cate_id: {2} '.format(name, school_id,
                                                                                                 school_major_cate_id))
            insert_str = 'insert into {0} VALUES (NULL, %s, %s, %s)'.format(db.settings['table']['school_major'])
            cursor.execute(insert_str, (name, school_id, school_major_cate_id))
            conn.commit()

    conn.close()
    cursor.close()


def get_old_enrollment_list(bs, school_url):
    en_list = []
    old_enrollment_items = bs.select('.s_top2 > .s_item > a[href^="?blue=action=show_jz"]')
    for old_enrollment_item in old_enrollment_items:
        href = old_enrollment_item.attrs['href']
        url = school_url + href
        html = utils.get_html(url)
        if not html:
            continue
        en_bs = BeautifulSoup(html, 'html.parser')
        title = en_bs.select('title')[0].get_text()
        content = en_bs.select('.s_content')[0].strip()
        ref_id = int(href.split('|')[2].split('=')[1])
        en_list.append((ref_id, title, content))

    return en_list


if __name__ == '__main__':
    main()
