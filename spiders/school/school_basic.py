# coding:utf-8
import requests
from bs4 import BeautifulSoup
import sys

reload(sys)
sys.setdefaultencoding('utf8')
import db
import json

# ####################日志设置#####################
import os, utils

log_path = os.path.dirname(os.path.abspath(__file__)) + '/log'
logger = utils.get_logger(log_path)


# ####################日志设置#####################

def main():
    page = 1
    basic_url = 'http://www.yikaosheng.com/school/ajax.php?dis=&score=&spc=&my_city=&my_kaodian=&my_chengji=&my_fav=&key=&page='

    conn = db.get_connection()
    cursor = db.get_cursor(conn)

    while 1:
        url = basic_url + str(page)
        try:

            data = utils.get_html(url)
            if not data:
                continue
            data = json.loads(data)[0:-1]

            for item in data:
                ref_id = item['id']
                name = item['name']
                logo = 'http://i.yikaoapp.com/upload/school/' + ref_id + '/' + item['logo']
                score = item['score_type']
                banner = None
                tag = item['tag']
                schoolType = None
                nature = None
                recruitOrder = None
                recruitTotalNum = None
                intro = None
                address = None
                telephone = None
                city = None
                district_id = None

                select_str = "select id from {0}".format(db.settings['table']['school']) + " where ref_id=%s"
                if cursor.execute(select_str, (ref_id,)):
                    logger.warning('ref_id: {0} exists'.format(ref_id))
                    continue

                logger.debug('insert: ' + ref_id + '\t' + name + '\t' + logo + '\t' + score + '\t' + tag)
                insert_str = "insert into {0}".format(db.settings['table'][
                                                          'school']) + " values (null, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
                cursor.execute(insert_str, (
                    ref_id, name, logo, score, banner, tag, schoolType, nature, recruitOrder, recruitTotalNum, intro,
                    address, telephone, city, district_id))
                conn.commit()

            page += 1
            if len(data) > 1:
                continue
            else:
                break
        except Exception as e:
            logger.error('Error url :{0}, e:{1}'.format(url, e))

    conn.close()
    cursor.close()


if __name__ == '__main__':
    main()
