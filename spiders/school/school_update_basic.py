# coding:utf-8
import requests
from bs4 import BeautifulSoup
import sys

reload(sys)
sys.setdefaultencoding('utf8')
import db
import json

# ####################日志设置#####################
import os, utils

log_path = os.path.dirname(os.path.abspath(__file__)) + '/log'
logger = utils.get_logger(log_path)


# ####################日志设置#####################

def get_intro(bs, school_url):
    items = bs.select('.s_top2 > .s_item > a')
    url = None
    for item in items:
        if item.get_text() == '学校简介':
            url = school_url + item.attrs['href']
            break
    if not url:
        return None

    html = utils.get_html(url)
    if not html:
        return None
    bs = BeautifulSoup(html, 'html.parser')
    intro = str(bs.select('.s_content')[0]).strip()
    return intro


def get_address(bs):
    items = bs.select('.s_top2 > .s_item > a')
    address = None
    for item in items:
        if '地址' in item.get_text():
            address = item.get_text().split('：')[1]
    return address


def get_telephone(bs):
    items = bs.select('.s_top2 > .s_item > a')
    telephone = None
    for item in items:
        if '电话' in item.get_text():
            telephone = item.get_text().split('：')[1]
    return telephone


def get_city(bs, school_tag):
    city = None
    items = bs.select('.s_tag > span')

    items_len = len(items)
    if items_len == 1:
        city = None
    elif items_len > 1 and school_tag == '':
        city = items[1].get_text()
    elif items_len > 1 and school_tag != '':
        tag_len = len(school_tag.split(','))
        if items_len - tag_len <= 1:
            city = None
        else:
            city = items[1].get_text()

    return city


def main():
    # 从数据库中获取所有学校id, ref_id数据
    conn = db.get_connection()
    cursor = db.get_cursor(conn)
    cursor.execute('select id, ref_id, tag from {0} ORDER BY id'.format(db.settings['table']['school']))
    schoolList = cursor.fetchall()
    for school in schoolList:
        school_id, school_ref_id, school_tag = school

        logger.debug('school_id: ' + str(school_id) + '\t' + 'school_ref_id: ' + str(
                school_ref_id) + '\t' + 'school_tag: ' + str(school_tag))
        # logger.debug('school_url: '+school_url)
        school_url = 'http://i.yikaosheng.com/school/school_' + str(school_ref_id) + '.html'

        html = utils.get_html(school_url)
        if not html:
            continue
        bs = BeautifulSoup(html, 'html.parser')
        schoolInfoItems = bs.select('.s_info > div')

        banner = bs.select('#topimg')[0].attrs['src']
        schoolType = schoolInfoItems[1].get_text().split('：')[1]
        nature = schoolInfoItems[2].get_text().split('：')[1]
        recruitOrder = schoolInfoItems[3].get_text().split('：')[1]
        recruitTotalNum = schoolInfoItems[4].get_text().split('：')[1]
        intro = get_intro(bs, school_url)
        address = get_address(bs)
        telephone = get_telephone(bs)
        city = get_city(bs, school_tag)

        logger.debug(
                'update: id: ' + str(school_id) + str(banner) + '\t' + str(schoolType) + '\t' + str(
                        nature) + '\t' + str(
                        recruitOrder) + '\t' + \
                str(recruitTotalNum) + '\t' + str(address) + '\t' + str(telephone) + '\t' + str(city))

        update_str = 'update {0} as t set t.banner=%s, t.schoolType=%s, t.nature=%s, recruitOrder=%s, recruitTotalNum=%s, intro=%s, address=%s, telephone=%s, city=%s where id=%s'.format(
                db.settings['table']['school'])
        cursor.execute(update_str, (
            banner, schoolType, nature, recruitOrder, recruitTotalNum, intro, address, telephone, city, school_id))
        conn.commit()

    conn.close()
    cursor.close()


if __name__ == '__main__':
    main()
