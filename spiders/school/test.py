# coding:utf-8
import requests
from bs4 import BeautifulSoup
import sys

reload(sys)
sys.setdefaultencoding('utf8')
import db
import json
import re
import time

# ####################日志设置#####################
import os, utils
log_path = os.path.dirname(os.path.abspath(__file__)) + '/log'
logger = utils.get_logger(log_path)
# ####################日志设置#####################

url = 'http://i.yikaoapp.com/d-53.html'


html = utils.get_html(url)
content = str(BeautifulSoup(html, 'html.parser').select('.wrap')[0])
logger.debug(content)