# coding:utf-8
import requests
from bs4 import BeautifulSoup
import sys

reload(sys)
sys.setdefaultencoding('utf8')
import db
import json

# ####################日志设置#####################
import os, utils

log_path = os.path.dirname(os.path.abspath(__file__)) + '/log'
logger = utils.get_logger(log_path)
# ####################日志设置#####################

import time


def main():
    basic_url = 'http://www.yikaosheng.com/school/ajax.php?dis=&score=&my_city=&my_kaodian=&my_chengji=&my_fav=&key=&spc='

    conn = db.get_connection()
    cursor = db.get_cursor(conn)

    # 先从数据库重查出学校分类列表
    cursor.execute('select id, ref_id from {0}'.format(db.settings['table']['school_cate']))
    schoolCateList = cursor.fetchall()

    for school_cate in schoolCateList:
        school_cate_id, school_cate_ref_id = school_cate[0], school_cate[1]
        # 遍历该分类下的所有学校
        page = 1
        while 1:
            url = basic_url + str(school_cate_id)
            url = url + '&page=' + str(page)
            logger.debug('url: {0}'.format(url))
            try:
                data = utils.get_html(url)
                if not data:
                    continue
                data = json.loads(data)[0:-1]
                for item in data:
                    ref_id = item['id']
                    # 判断school表中是否有该记录，有的话，取出该记录对应的学校id
                    if not cursor.execute(
                            'select id from {0} where ref_id=%s limit 1'.format(db.settings['table']['school']),
                            (ref_id,)):
                        logger.warning('ref_id: {0} is not exists in school'.format(ref_id))
                        continue
                    school_id = cursor.fetchall()[0][0]

                    # 判断关系表school_and_cate表里面是否已经有该条记录，有的话就不做操作
                    select_str = "select id from {0}".format(
                            db.settings['table']['school_and_cate']) + " where school_id=%s and school_cate_id=%s"
                    if cursor.execute(select_str, (school_id, school_cate_id)):
                        logger.warning(
                                'school_id: {0} and school_cate_id:{1} is already exists in school_and_cate'.format(
                                        school_id, school_cate_id))
                        continue
                    # 如果没有，插入该条记录
                    logger.debug('insert: school_id: {0}, school_cate_id: {1}'.format(school_id, school_cate_id))
                    insert_str = 'insert into {0} values(null, %s, %s)'.format(db.settings['table']['school_and_cate'])
                    cursor.execute(insert_str, (school_id, school_cate_id))
                    conn.commit()
                page += 1
                if len(data) > 1:
                    continue
                else:
                    break
            except Exception as e:
                logger.error('Error url :{0}, e:{1}'.format(url, e))

    conn.close()
    cursor.close()


if __name__ == '__main__':
    main()
