#coding:utf-8
import requests
from bs4 import BeautifulSoup
import sys
reload(sys)
sys.setdefaultencoding('utf8')
import db
import json
from datetime import date

# ####################日志设置#####################
import os, utils
log_path = os.path.dirname(os.path.abspath(__file__)) + '/log'
logger = utils.get_logger(log_path)
# ####################日志设置#####################

def get_majorBoxItem(bs):
    item = None
    s_tips = bs.select('.s_tips')
    for tip in s_tips:
        if tip.get_text() == '招生专业':
            item = bs.select('.s_top2')[0]
            break
    return item

def main():
    #从数据库中获取所有学校id, ref_id数据
    conn = db.get_connection()
    cursor = db.get_cursor(conn)
    cursor.execute('select id, ref_id from {0} ORDER BY id'.format(db.settings['table']['school']))
    schoolList = cursor.fetchall()
    for school in schoolList:
        school_id, school_ref_id = school

        logger.debug('school_id: '+str(school_id) + '\t' + 'school_ref_id: '+ str(school_ref_id))
        school_url =  'http://i.yikaosheng.com/school/school_'+str(school_ref_id)+'.html'

        html = utils.get_html(school_url)
        if not html:
            continue
        bs = BeautifulSoup(html, 'html.parser')

        #处理简章数据
        old_enrollment_list = get_old_enrollment_list(bs, school_url)
        for old_enrollment_item in old_enrollment_list:
            ref_id, title, content = old_enrollment_item
            select_str = 'select id from {0} where ref_id=%s'.format(db.settings['table']['school_enrollment'])
            if cursor.execute(select_str, (ref_id, )):
                logger.warning('enrollment_ref_id {0} is already exists !'.format(ref_id))
                continue

            logger.debug('insert: ref_id: {0}, title: {1}, school_id: {2}'.format(ref_id, title, school_id))
            insert_str = 'insert into {0} VALUES (NULL, %s, %s, %s, %s, %s)'.format(db.settings['table']['school_enrollment'])
            cursor.execute(insert_str, (ref_id, title, content, school_id, 0))
            conn.commit()

        #处理分数线数据
        score_list = get_score_list(bs, school_url)
        for score_item in score_list:
            ref_id, title, content = score_item
            select_str = 'select id from {0} where ref_id=%s'.format(db.settings['table']['school_score'])
            if cursor.execute(select_str, (ref_id, )):
                logger.warning('score_ref_id {0} is already exists !'.format(ref_id))
                continue

            logger.debug('insert: ref_id: {0}, title: {1}, school_id: {2}'.format(ref_id, title, school_id))
            insert_str = 'insert into {0} VALUES (NULL, %s, %s, %s, %s)'.format(db.settings['table']['school_score'])
            cursor.execute(insert_str, (ref_id, title, content, school_id))
            conn.commit()

    conn.close()
    cursor.close()

def get_old_enrollment_list(bs, school_url):
    en_list = []
    old_enrollment_items = bs.select('.s_top2 > .s_item > a[href^="?blue=action=show_jz"]')
    for old_enrollment_item in old_enrollment_items:
        href = old_enrollment_item.attrs['href']
        url = school_url + href
        html = utils.get_html(url)
        if not html:
            continue
        en_bs = BeautifulSoup(html, 'html.parser')
        title = en_bs.select('title')[0].get_text()
        logger.debug('title: '+title)

        content = en_bs.select('.s_content')
        if content:
            content = str(content[0]).strip().replace('src="/', 'src="http://i.yikaosheng.com/')
            ref_id = int(href.split('|')[2].split('=')[1])
            en_list.append((ref_id, title, content))

    return en_list

def get_score_list(bs, school_url):
    score_list = []
    score_items = bs.select('.s_top2 > .s_item > a[href^="?blue=action=show_score"]')
    for score_item in score_items:
        href = score_item.attrs['href']
        url = school_url + href
        html = utils.get_html(url)
        if not html:
            continue
        en_bs = BeautifulSoup(html, 'html.parser')
        title = en_bs.select('title')[0].get_text()
        logger.debug('title: '+title)

        content = en_bs.select('.s_content')
        if content:
            content = str(content[0]).strip()
            ref_id = int(href.split('|')[2].split('=')[1])
            score_list.append((ref_id, title, content))

    return score_list

if __name__ == '__main__':
    main()